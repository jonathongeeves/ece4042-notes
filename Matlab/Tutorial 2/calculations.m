clc
clear all
close all

%% Question 2

f = -10e3:10e3;
g = 10e-6 * f.^2;

fig = figure();
plot(f, g, 'k-')
%hold on
xlabel('f (Hz)');
ylabel('G_x(f)')

hgexport(fig, '../../Images/T2Q2.eps')

%% Question 3
fig = plot_bandwidth(0);
hgexport(fig, '../../Images/T2Q3_1.eps')

% Part a
x = 0.44295;
x = x/10^-4
2*x/1000

fig = plot_bandwidth(x);
hgexport(fig, '../../Images/T2Q3_2.eps')

% Part b
W = 10^4;
fig = plot_bandwidth(W/2);
hgexport(fig, '../../Images/T2Q3_3.eps')

% Part c
W = 2*10^4;
fig = plot_bandwidth(W/2);
hgexport(fig, '../../Images/T2Q3_4.eps')

%Part d
target = 0.495/10^-4

sum = 0;
f_0 = 0;

while sum < target
   sum = sum + sinc(f_0*10^-4); 
   f_0 = f_0 + 1;
end
f_0

fig = plot_bandwidth(f_0);
hgexport(fig, '../../Images/T2Q3_5.eps')


% Part e
