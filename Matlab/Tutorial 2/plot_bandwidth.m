function [ fig ] = plot_bandwidth( half_bandwidth )
f = (10^6 - 3*10^4):(10^6 + 3*10^4);
G = @(f) 10^-4 * sinc((f - 10^6) * 10^-4).^2;
fig_pos = [1000, 1000, 800, 300];

fig = figure();
hold on
set(fig, 'Position', fig_pos)
plot(f, G(f), 'k-');
xlabel('f');
ylabel('G_x(f)');

if half_bandwidth > 0
    for f_val = (10^6 - half_bandwidth):10:(10^6 + half_bandwidth)
        plot([f_val f_val], [0 G(f_val)], 'r-');
    end
end

end

