clc
clear all
close all

%% Question 2
P_t_0 = 0.3;
P_t_1 = 0.7;
P_e = 0.2;

P_r_1 = P_t_1 * (1 - P_e) + P_t_0 * P_e
P_2 = P_t_1 * (1 - P_e) / P_r_1

%% Question 4
P_X = @(k) nchoosek(4, k) * (1 / 4)^k * (3 / 4)^(4 - k);
P_Y = @(k) nchoosek(4, k) * (1 / 2)^4;

P_1 = P_X(2) * P_Y(2)

P_2 = 0;
for k = 0:4
    P_2 = P_2 + P_X(k) * P_Y(k);
end
P_2

P_3 = 0;
for k = 0:3
    P_x_greater_k = 0;
    for i = (k+1):4
       P_x_greater_k = P_x_greater_k + P_X(i); 
    end
    P_3 = P_3 + P_Y(k) * P_x_greater_k;
end
P_3

P_4 = P_X(2) * P_Y(4) + P_X(3) * (P_Y(3) + P_Y(4)) + P_X(4) * (P_Y(2) + P_Y(3) + P_Y(4))
P_5 = 1 - P_4