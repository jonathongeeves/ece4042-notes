clc
clear all
close all

%% Question 5.
Eb_No = 0:0.1:20;

P_b = qfunc(sqrt(Eb_No/2));

f = figure()
semilogy(10*log10(Eb_No), P_b)
xlabel('E_b/N_0 (dB)');
ylabel('P_B');
hgexport(f, '../../Images/Pb_vs_EbNo.eps');