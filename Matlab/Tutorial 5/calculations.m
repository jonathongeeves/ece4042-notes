clc
clear all
close all

%% Question 1
qfunc(2)
x = qfuncinv(6*10^-3)
x = x^2
x = x/2

%% Question 3
P_B = 2*10^-5 / log2(4)
P_B = 9*10^-3 / log2(8)
P_B = 1.6*10^-4 / log2(16)