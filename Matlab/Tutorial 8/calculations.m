clc
clear all
close all

%% Question 1.
a = [6 -3 2 1];
tau = [0 1 2 6];

% Mean delay spread.
tau_mean = sum(a.^2 .* tau) / sum(a.^2)

% RMS Dlay spread
sigma_t = sqrt( sum(a.^2 .* (tau - tau_mean).^2) / sum(a.^2))

%% Question 3.
x_start = [1 1 1 0];
x_old = x_start;
x_new = [0 0 0 0]; % Default initialisation.

k = 1;
while 1 == 1
    x_new(1) = mod(x_old(3) + x_old(4), 2);
    x_new(2) = x_old(1);
    x_new(3) = x_old(2);
    x_new(4) = x_old(3);
    
    fprintf('%d & %d & %d & %d & %d \\\\ \n', k, x_new(1), x_new(2), x_new(3), x_new(4));
    
    c(k) = x_old(4);
    
    if x_new == x_start
        break
    else
        x_old = x_new;
    end
    k = k + 1;
end

for k = 1:length(c)-1
    shift = circshift(c', k);
    a = sum(shift == c')
    d = sum(shift ~= c')
    a - d
end